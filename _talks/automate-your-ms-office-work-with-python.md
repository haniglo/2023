---
duration: 25
presentation_url:
room:
slot:
speakers:
- Bruno Gonçalves
title: "Automate your (MS) Office work with Python"
type: talk
video_url:
---
A hands-on look at how we can leverage Python to automatically interact with
MS Office applications and generate reports (Word), spreadsheets (Excel),
and presentations (PPT) directly from Python while leveraging Python's web
scraping and data wrangling capabilities. The talk will work through a
specific example where a public dataset is analyzed and different
deliverables are produced directly from within a Jupyter Notebook
