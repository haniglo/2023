---
duration: 25
presentation_url:
room:
slot:
speakers:
- Jan Giacomelli
title: "Celery on AWS ECS - the art of background tasks \u0026 continuous deployment"
type: talk
video_url:
---
This talk presents techniques, approaches, and practical configuration
examples that allow you to run your Celery workers on top of AWS ECS with
Fargate. It starts by presenting the problems you encounter when dealing
with async tasks. Next, we'll take a look at ECS and its configuration
examples - which parameters to tweak to achieve the best result. After that,
we'll check Celery configuration examples that comply with ECS requirements.
We'll finish by overviewing techniques for implementing tasks in a way that
supports ECS + Celery configuration while allowing zero downtime deploying
all the time.
