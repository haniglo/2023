---
duration: 10
presentation_url:
room:
slot:
speakers:
- Pamela Fox
title: "Portrait of a Python Program"
type: talk
video_url:
---
How many ways can you visualize a Python program? As a puzzle? A stack? A
pile of frames? A bunch of disassembled gobbledygook? Go on this journey
with me as I explore PythonLand from multiple perspectives.
