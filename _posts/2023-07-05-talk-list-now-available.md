---
title: Talk list now available!
date: 2023-07-05 10:00:00 -0400
excerpt_separator: <!--more-->
---

The PyGotham TV 2023 [talk list](/talks/) is now available. It features 33
talks spread across October 6th and 7th.

<!--more-->

This year's program features a single track with more than 35 speakers,
hosts, and presenters giving talks covering a wide array of topics, from
application design to data science to natural language processing to web
development.

We're really excited about this year's talks and will have a schedule
ready in the weeks leading up to the conference.

Thanks to everyone who proposed a talk and voted on talks.
