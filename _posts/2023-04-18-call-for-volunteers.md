---
title: Call for Volunteers
date: 2023-04-18 08:00:00 -0400
---

As we mentioned in our [conference announcement]({% post_url
2023-03-04-announcing-pygotham-2023 %}), the PyGotham organizers are looking for
new volunteers to join the team. If you're interested in volunteering for a
specific conference planning task or contributing as an organizer throughout the
planning cycle, we invite you to join an info session with the team. We'll share
information about many of the organizer roles we have today and some we'd like
to add in the future, and we'll answer any questions you have about the work
involved in making a conference like this one happen. We'll be hosting two of
these sessions this month: Tuesday, April 25th at 8:00PM EDT and Saturday, April
29th at 12:00PM EDT. To join, email <volunteers@pygotham.org> and mention which
session works best for you, and we'll respond with a calendar invite and video
call link.
