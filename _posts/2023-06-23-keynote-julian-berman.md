---
title: "Keynote Speaker: Julian Berman"
date: 2023-06-23 00:00:00 -0400
image: /uploads/posts/julian-berman.jpg
excerpt_separator: <!--more-->
---

Julian Berman is a software developer and open source software maintainer who
currently serves as a Technical Lead, API Specifications at Postman, where he is
graciously funded to work on open source projects, most notably the JSON Schema
specification(s) and his implementation of them in Python. <!--more--> He
previously served as Vice President of Engineering for Decisioning Automation at
Deloitte Digital, overseeing teams of engineers and data scientists delivering
machine learning solutions to large clients.
