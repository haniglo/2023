---
title: The Call for Proposals is now open!
date: 2023-04-14 08:00:00 -0400
---

The [Call for Proposals]({{ site.data.event.cfp_url }}) is now open. Talk and
film proposals are due by May 15, 2023.
