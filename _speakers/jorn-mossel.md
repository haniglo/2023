---
name: Jorn Mossel
talks:
- "When is extreme too extreme? A Bayesian approach to modeling NYC rainfall with PyMC"
---
Jorn obtained his Ph.D. from the University of Amsterdam in the field of
Theoretical Physics, during which he published 8 peer-reviewed papers on
quantum many-body systems out of equilibrium. Jorn then moved to London to
join Goldman Sachs to work on systematic investment strategies and later
joined a hedge fund to end up of as head of equity risk research.
