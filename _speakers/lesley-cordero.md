---
name: Lesley Cordero
talks:
- "Microservice Observability with Python and OpenTelemetry"
---
Lesley Cordero is currently a Staff Software Engineer at The New York Times.
She has spent the majority of her career on edtech teams as an engineer,
including Google for Education and other edtech startups.

In her previous roles she has focused on building robust data pipelines,
setting technical strategy, building excellent engineering teams &
communities, and reliability management. Some more specifics include setting
org-wide vision & strategy for observability, improving on-call processes,
adopting chaos engineering practices, and cultivating culture that builds
with the most vulnerable employees in mind first.

She shows care for others by holding them accountable to the best versions
of themselves – and by buying them the occasional bubble tea.
