---
name: Joshua Cannon
talks:
- "A BuildEngineer in a buildless lang"
---
Josh is an avid lover of tooling that accelerates developers, including
being a maintainer of the Polyglot Open-Source Build System: Pants.

In his free time, Josh enjoys laser cutting/engraving, reading Brandon
Sanderson, and his wonderful family.
