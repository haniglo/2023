---
name: Mario Munoz
talks:
- "Python2Nite with Mario Munoz"
---
I code Python by night, which is what happens when there's not enough time
during the day. In the past couple of years, I've presented several
talks/tutorials at PyCon US, DjangoCon US, Python Web Conference, and
others. Sometimes I neglect/blog on my website [Python By
Night](https://pythonbynight.com), and start (and abandon) too many side
projects.
