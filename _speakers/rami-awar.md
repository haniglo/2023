---
name: Rami Awar
talks:
- "Exploring Functional Options in Python"
---
Rami Awar is a senior backend developer based in Amsterdam. He's worked with
multiple backend frameworks and languages, and tries to compile thoughts
that bring them all together in his blog https://SoftGrade.org.  He is
currently working on https://DataLine.app, to be released soon.
